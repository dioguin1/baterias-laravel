
@extends ('templates.base')

@section ('conteudo')
    <main>
        <h1>Medições</h1>
        <hr>
        <h2>Valores obtidos:</h2>
        <table class="table table-stiped  table-bordered" id="tbDados">
            <thead>
            <tr>
                <th> Bateria </th>
                <th> Tensão nominal </th>
                <th> Capacidade de corrente </th>
                <th> Tensão sem carga </th>
                <th> Tensão com carga </th>
                <th> Resistência de carga "OHM"</th>
                <th> Resistência interna "OHM"</th>
        
            </tr>
            </thead>
            <tbody>
                @foreach ($medicoes as $medicao)
                    
               
                <tr>
                    <td>  { {$medicao->pilha_bateria}}</td>
                    <td> {{number_format($medicao->tensao_nominal,1,'.','')}}</td>
                    <td> {{$medicao->capacidade_corrente}}</td>
                    <td> {{$medicao->tensao_sem_carga}}</td>
                    <td>{{$medicao->tensao_com_carga}}</td>
                    <td> {{$medicao->resistencia_carga}}</td>
                    <td> {{number_format($medicao->resistencia_interna,3,'.','')}}</td>
                </tr>
                @endforeach
            </tbody>
 
        </table>
    </main>
    @endsection

@section('rodape')
<h4>Rodape </h4>
@endsection