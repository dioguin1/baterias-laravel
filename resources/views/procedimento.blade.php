@extends ('templates.base')

@section ('conteudo')
    <main>
        <h1>Procedimento</h1>
        <hr>
        <h2>Medições:</h2>
        <p>
            1° passo : Separação dos materiais: multímetro, baterias e pilhas.<br>
            2° passo : Com o multímetro, foi feita a medição da tensão interna (E) e da tensão com carga (V) das pilhas e das baterias que foram apresentadas no laboratório.<br>
            3° passo : Calculamos o valor real do resistor (R), utilizado como carga.<br>
            4° passo: Após as medições, anotamos o valor de cada pilha e cada bateria (totalizando 10) para determinar a resistência interna (r) e suas "vida útil".<br>
            5° passo : Para saber a vida útil das pilhas/baterias, foi necessários medirmos a tensão, juntamente com a resistência interna.<br>

        </p>
    </main>
@endsection

@section('rodape')
    <h4>Rodape procedimentos</h4>
@endsection
